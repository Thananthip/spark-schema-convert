package org.zalando.spark.jsonschema

import org.apache.spark.sql.AnalysisException
import org.apache.spark.sql.types._
import org.scalatest.{ BeforeAndAfter, FunSuite, Matchers }
import org.zalando.spark.jsonschema.SparkTestEnv._

import scala.util.Try

class SchemaConverterTest extends FunSuite with Matchers {

  val expectedStruct = StructType(Array(
    StructField("c1", StringType, nullable = true),
    StructField("c2", IntegerType, nullable = true),
    StructField("c3", LongType, nullable = true),
    StructField("c4", DoubleType, nullable = true),
    StructField("c5", StringType, nullable = false)
  ))

  test("should convert schema.json into spark StructType") {
    val testSchema = SchemaConverter.convert("D:\\True_Tap\\Lib\\spark-schema-convert\\src\\test\\resources\\sample_file.json")
    println(testSchema)
    assert(testSchema === expectedStruct)
  }
}