name := "spark-json-schema"

version in ThisBuild := "0.6.1"
organization := "org.zalando"

scalaVersion := "2.11.12"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.1.0"  % Provided
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.10"
dependencyOverrides ++= Set("com.fasterxml.jackson.core" % "jackson-databind" % "2.6.5")

//libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

scapegoatVersion := "1.3.0"
scapegoatIgnoredFiles := Seq(s"${target.value}.*.scala")